#include "minigrep.h"
#include <limits.h>

int main(int argc,char** argv)
{
	static pattern* pat;
	static FILE* inp;
	static char inp_buf[1024];
	static char* exp_buf = "^str*";

	if (CHAR_BIT != 8)
	{
		fprintf(stderr,"Error: requires 8-bit bytes\n");
		return 1;
	}
	
	if (!(pat = MakePattern(exp_buf)))
	{
		fprintf(stderr,"Can't make expression temlate\n");
		return 1;
	}
	inp = stdin;
	while (fgets(inp_buf,sizeof(inp_buf),inp))
	{
		if(MatchString(inp_buf,pat,0))
			fputs(inp_buf,stdout);
	}

	return 0;
}